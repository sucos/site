---
Title: Live Server
Weight: 3
Params:
    bg: bg-gradient-red
    text-color: text-light
---

Say goodbye to manual reloads. **SuCoS** offers a live server feature that lets you see your changes as they happen. Just save your file, and watch your site transform in real time.
