---
Title: Simplicity Meets Flexibility
Weight: 2
Params:
    bg: bg-gradient-green
    text-color: text-white
---

Ease of use doesn't mean you have to compromise on flexibility. **SuCoS** leverages the power of **Liquid templates**, providing an intuitive, easy-to-use templating system that won't limit your creativity.
