---
Title: Getting Started
Weight: 5
Params:
    bg: bg-gradient-orange
    text-color: text-white
---

Your journey to faster, simpler web development begins here.

Check out our [Get Started Guide 2](/docs) for a comprehensive walkthrough on setting up your first SuCoS project.
