---
Title: Weekly Releases
Weight: 4
Params:
    bg: bg-gradient-purple
    text-color: text-white
---

We believe in moving forward. We offer **weekly releases** to ensure you're always working with the most cutting-edge tools. Our dedication to fast, frequent updates means you'll never be left behind.

:::btn
[![Latest release](https://gitlab.com/sucos/sucos/-/badges/release.svg)](https://gitlab.com/sucos/sucos)
![Latest Pipepline](https://gitlab.com/sucos/sucos/badges/main/pipeline.svg?ignore_skipped=true)
:::
