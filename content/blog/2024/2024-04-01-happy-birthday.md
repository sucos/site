---
Title: "SuCoS Turns 1"
Date: 2024-04-01
Tags:
  - CSharp
  - Birthday
  - AI
---

## Past

Hold onto your hats, folks! It's time to celebrate a major milestone - our very own Static Site Generator (SSG) built with C# has officially turned one! As we blow out the candles and sing "Happy Birthday," let's take a moment to reflect on the wild ride that our SSG has been on since its inception on April 1st, 2023.

Our SSG was born out of a desire to test and improve my C# skills, as well as a growing frustration with the template system used by [Hugo](https://gohugo.io/), a popular SSG built with Golang. I wanted to create a solution that addressed a problem that I was personally facing. And so, the idea of our SSG was conceived!

## Present

When it came to choosing a template system for our SSG, I opted for [Liquid templates](https://shopify.github.io/liquid/). Their popularity and user-friendly nature made them an ideal choice for our SSG. They provided the flexibility and ease of use that I was looking for, making it a perfect fit for our SSG.

As for performance, [DotNet](https://dotnet.microsoft.com/) (6, 7, and now, 8) has been instrumental in making our SSG the powerhouse that it is today. The C# team's continued efforts to enhance the performance of DotNet have made it a viable option for performance-critical programs. This has significantly improved the speed and efficiency of our SSG, making it a reliable choice for developers.

As mostly a one-man project, the use of AI was a very important motivator for diving into unknown territory. [ChatGPT](https://chat.openai.com/), [Copilot](https://copilot.github.com/), [Bard](https://bard.ai/), [Gemini](https://gemini.google.com), and [Mistral](https://chat.mistral.ai) were mainly used to generate test suites, analyze and suggest optimizations in the code, and suggest libraries that might help.

## Future

While our SSG has come a long way in its first year, there's still work to be done. In the coming months, I plan to implement key features such as pagination, multi-language, and image manipulation and conversion. My "ultimate" goal is to port my [personal blog](https://brunomassa.com) to our SSG, and I'm confident that these improvements will make that possible.

My wife thought it was an April Fool's joke when I started our SSG. But here we are, a year later, celebrating the first birthday of a project that has grown beyond my wildest dreams. It's been a year of learning, growth, and innovation, and I can't wait to see what the future holds for our SSG.
