---
Title: Homepage
---

![SuCoS logo](/SuCoS-logo.svg){: .img-fluid} {: .text-center}

> [!IMPORTANT]
> The world’s fastest framework for building websites.

**SuCoS** is a brand new **open-source** static site generators crafted in **C#**. With its amazing speed and flexibility, SuCoS makes building websites fun again.
