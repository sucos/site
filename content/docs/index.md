---
Title: Docs
---

SuCoS is a minimalist Static Site Generator focused on simplicity and speed. It converts your Markdown content into a fully functional website, with minimal configuration required.

## Prerequisites

None! SuCoS is distributed as a single executable file that works right out of the box.

## Quick Start

1. Install SuCoS following the [installation guide](installation).

2. Create a new site:
```sh
sucos new-site ./my-site
cd my-site
```

3. Add some content:
   - For a regular page: Create a file at `content/pages/my-page.md`
   - For a blog post: Create a file at `content/blog/my-post.md`

4. Preview your site:
```sh
sucos serve
```
Then visit `http://localhost:2341` in your browser to see your site.

5. (Optional) Create a custom theme:
```sh
sucos new-theme
```
This will create a new theme in the `themes` directory that you can customize.

6. (Optional) Publish your site:
   - Build the site: `sucos build`
   - The static files will be in the `public` directory
   - Deploy these files to your preferred hosting service

## What's Next?

- Learn more about [content creation](/docs/content)
- Explore [theming](/docs/theme)
