---
Title: Site Variables
Params:
  hasCode: true
---

In SuCoS, the liquid templating language provides access to various site variables, empowering you to create dynamic and personalized websites.

These are just a few examples of the site variables available in SuCoS through the power of liquid templating. By harnessing these variables, you can create dynamic and personalized websites that adapt to your specific needs. Experiment with these variables to unlock the full potential of SuCoS and take your website to new heights of customization and interactivity.

Remember, you can access site-wide variables and values through `page.Site`, as all pages point to the site.

Let's dive into some of the essential site variables and their functionalities:

## BaseURL

```html
{{ Site.BaseURL }}
```

The `Site.BaseURL` variable stores the base URL of your site. It can be useful when you need to construct absolute links within your templates. By appending this variable to relative links, you can ensure that they point to the correct location.

## Copyright

```html
{{ Site.Copyright }}
```

## Home

```html
{{ Site.Home }}
```

The `Site.Home` variable refers to the homepage page object. You can access properties of the homepage page using this variable. For example, {{ site.Home.Title }} retrieves the title of the homepage.

## Pages

```html
{{ Site.Pages }}
```

The `site.Pages` variable represents all pages on your site, including regular pages, sections, and taxonomy pages. You can iterate over this collection using a liquid for loop to perform operations on each page.

For example, to print the title of each page on your site, you can use the following liquid for loop:

```html
<ul>
{% for page in site.Pages %}
  <li>{{ page.Title }}</li>
{% endfor %}
</ul>
```

This will iterate over all pages and output their titles.

## Params

```html
{{ Site.Params }}
```

The `Site.Params` variable holds custom parameters defined in your site configuration. You can utilize these parameters to store and access additional site-specific information. For example, if you have a parameter named author in your site configuration, you can access its value using {{ site.Params.author }}.

## RegularPages

```html
{{ Site.RegularPages }}
```

The `site.RegularPages` variable provides access to a collection of regular pages on your site. This collection is the same as [`site.Pages`](#pages), but it filters out pages with `pages.Kind == Kind.single`. You can iterate over this collection to perform operations on each individual page, such as displaying a list of all regular pages or filtering pages based on specific criteria.

For example, to print the title of each regular page, you can use the following liquid for loop:

```html
{% for page in site.RegularPages %}
  {{ page.Title }}
{% endfor %}
```

This will iterate over each regular page and output its title.

## Title

```html
{{ Site.Title }}
```

The `Site.Title` variable represents the title of your website. It allows you to dynamically display the name of your site throughout different templates.
