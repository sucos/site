---
Title: Page Variables
Params:
  hasCode: true
---

In SuCoS, page variables from the front matter allow you to define metadata and properties for individual content Markdown files. They provide a structured way to specify various attributes and settings, enabling you to create dynamic and personalized websites. Let's explore the available front matter properties and their functionalities.

## Variables from Front Matter

The page front matter in SuCoS allows you to define metadata and properties for individual content Markdown files. It provides a structured way to specify various attributes and settings. It's super easy to use.

```yaml
---
Title: My page
Type: blog post
Draft: true
---

Content of the page
```

Below are the available properties and their descriptions:

### ContentRaw

The raw content from the Markdown file. It contains the original Markdown text without any modifications.

### Date

The `Date` variable represents the date of the content, typically used for blog posts or time-sensitive content.

### ExpiryDate

The `ExpiryDate` variable indicates the expiration date of the content.

### Lastmod

The `Lastmod` variable represents the last modification date of the content.

### Params

The `Params` variable is a special property that can absorb any custom variables or values not covered by standard front matter properties. It allows users to define and access their own custom data within the content files.

### PublishDate

The publication date of the content. If not specified, it defaults to the Date value.

### RawContent

The `RawContent` variable is the markdown content from each file AS-IS, before even being converted to HTML.

### Tags

The `Tags` property is a list of tags associated with the content. Tags are useful for categorizing and organizing content. They can be used to create tag-based navigation or filtering. In templates, you can iterate over the tags using a liquid for loop to display them.

In addition to the front matter properties, SuCoS also provides calculated page variables that are filled internally and not read from the content front matter. These variables can be utilized in templates for further customization.

```liquid
<ul>
    {% for tag in page.Tags %}
    <li><a href="{{ tag.Permalink }}">{{ tag.Title }}</a></li>
    {% endfor %}
</ul>
```

### Title

The title of the content. It represents the main heading or title for the page.

### Type

Represents the type of content. If not specified, it defaults to the value of Section. This property can be used to classify content based on different types, such as blog posts, documentation, or tutorials.

### URL

Specifies the URL pattern to be used in creating the content's URL. It allows you to define custom URL structures or placeholders for dynamic URLs.

### Weight

A numeric value used for sorting content pages.

## Variables Calculated

These following properties are filled internally by SuCoS itself and not read from the content front matter. It can be used normally in the templates.
### Aliases

The `Aliases` variable holds a list of alternative URLs that redirect to the content.

### Content

The processed content. It represents the final content that will be rendered on the page. This can include any modifications or additional processing applied to the original Markdown content.

### ContentPreRendered

The pre-rendered Markdown content. It represents the Markdown content that has been parsed and converted to HTML or any other pre-rendered format.

### IsHome

A boolean value indicating whether the current page is the home page.

### IsNode

A boolean value indicating whether the current page is a node in the content hierarchy.

### IsSection

A boolean value indicating whether the current page is a section page.

### Kind

Specifies the type of the page, whether it's a single page, a list of pages, or the home page. This property is useful for determining the page's layout or behavior based on its kind.

### OutputFormats

A list of available output formats for the current page.

### Pages

A collection of other content that mentions this content. Used on list pages like tag or section pages.

The page front matter allows you to customize various aspects of your content, such as the title, URL, tags, and more. These properties can be defined at the beginning of your Markdown file within a YAML block. They enable you to create dynamic and personalized websites using SuCoS SSG.

```html
<h1>{{ page.Title }}</h1>

<span>URL: {{ page.Permalink }}</span>
<span>Date: {{ page.Date }}</span>

Tags:
<ul>
    {% for tag in page.Tags %}
    <li>{{ tag.Title }}</li>
    {% endfor %}
</ul>

<div>
    {{ page.Content }}
</div>
```

### Parent

Reference to the parent page in the content hierarchy.

### Permalink

The permalink represents the URL for the content. It can be used to set a specific URL for the page, overriding the URL pattern.

### Plain

The content with all HTML tags stripped out.

### RelPermalink

The relative URL of the page (without the domain name).

### Section

Indicates the directory where the content is located. It provides information about the content's location within the project structure.

### Site

It point to the [site object](/docs/theme/variables/site-variables) and it's general properties. So, from each page, the site configuration can be accessed in the template.

### Tags

The `Tags` property is a list of tags associated with the content. Tags are useful for categorizing and organizing content. They can be used to create tag-based navigation or filtering. In templates, you can iterate over the tags using a liquid for loop to display them.

### URL

Specifies the URL pattern to be used in creating the content's URL. It allows you to define custom URL structures or placeholders for dynamic URLs.

### WordCount

The number of words in the content.
