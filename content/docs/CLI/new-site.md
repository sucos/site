---
Title: New Site
Weight: 1
Params:
  hasCode: true
---

The `SuCoS new-site` command allows you to create a simple folder structure to host your site or blog. This command generates a basic file and directory layout to help you get started with your SuCoS site.

## Option Details

Here are the command line options available for the `new-site` command:

- `--output` or `-o`: Specifies the output directory path. This is the location where SuCoS will create the new site structure. If not specified, the default output directory is the current directory (`./`).

- `--force` or `-f`: Forces site creation. Use this option to overwrite existing files or directories with the same name as the new site structure.

- `--title`: Sets the site title. This option allows you to specify the title of your site, which will be used in the site configuration file (`sucos.yml`). The default title is "My Site".

- `--description`: Sets the site description. This option allows you to specify a brief description of your site, which will be used in the site configuration file (`sucos.yml`). By default, the description is an empty string.

- `--url`: Sets the site base URL. This option allows you to specify the base URL of your site, which will be used in the site configuration file (`sucos.yml`). The default base URL is "<https://example.org/>".

## Directory Structure

When you run the `new-site` command, SuCoS will create the following directory structure:

```tree
└── your-site-name/
    ├── content/
    ├── static/
    ├── themes/
    └── sucos.yml
```

- `content/`: This directory is used to store your site's content, such as pages and blog posts.
- `static/`: This directory is used to store your site's static files, such as images, CSS, and JavaScript files.
- `themes/`: This directory is used to store your site's themes. You can create custom themes or use existing ones to change the look and feel of your site.
- `sucos.yml`: This file holds basic site information and configuration settings. For more information, see the [Site Configuration](/docs/content/site) manual.

## Command Line Usage Examples

Here are five examples showcasing the usage of the **SuCoS** new-site command, demonstrating the combination of different arguments:

### Create a new site with default settings

```bash
SuCoS new-site
```

This command will create a new site with default settings in the current directory.

### Create a new site with a custom output directory

```bash
SuCoS new-site -o ./my-site
```

This command will create a new site in the `./my-site` directory.

### Create a new site with a custom title and description

```bash
SuCoS new-site --title "My Blog" --description "A blog about my life"
```

This command will create a new site with a custom title ("My Blog") and description ("A blog about my life").

### Create a new site with a custom base URL

```bash
SuCoS new-site --url "https://example.org/"
```

This command will create a new site with a custom base URL ("<https://example.org/>").

### Create a new site with force enabled

```bash
SuCoS new-site -f
```

This command will create a new site, overwriting any existing files or directories with the same name. Be cautious when using this option, as it may result in data loss.
