---
Title: Terminal Commands CLI
Weight: 100
Params:
  hasCode: true
---

**SuCoS** offers various terminal commands to create, build, and serve your static site. These commands come with options that customize the website generation or server operation.

## Available Commands

* `build`: Build the static site based on your source files.
* `serve`: Start a local development server to preview your site.
* `new-site`: Generate a simple site from scratch.
* `new-theme`: Generate a simple theme from scratch.

## Common Options

The following options are available for most SuCoS commands:

* `--source` or `-s`: Specifies the source directory path. Use this option to provide a custom directory containing your website's source files. If not specified, the current directory is used as the source directory.
* `--verbose` or `-v`: Enables verbose output during the process. With this option, **SuCoS** provides detailed information and feedback, enhancing your understanding of the steps performed.
* `--help` or `-h` or `-?`: Show help and usage information.
* `--version`: Show version information (which in fact is displayed every time)

## Basic Commands

### New Site

```bash
SuCoS new-site
```

This command will create a new site with default settings in the current directory.

### New Theme

```bash
SuCoS new-theme
```

This command will create a new theme with default settings in the current directory.

### Basic Build Command

```bash
SuCoS build
```

This will build the site using the default source directory (`./`) and output the generated files to the default output directory (`./public`).

### Basic Serve Command

```bash
SuCoS serve
```

This command will start a local development server using the default source directory (`./`) and serve the site from memory.

## Enable Verbose Output

```bash
sucos build --verbose
```

This command will build the site with verbose output enabled, providing detailed information about the process.

For more information on each command and its specific options, please refer to their respective documentation pages: [New Site](new-site), [New Theme](new-theme), [Build](build), and [Serve](serve).
