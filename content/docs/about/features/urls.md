---
Title: Custom URLs
Params:
  hasCode: true
---

## Permalink

In **SuCoS**, the permalink determines the structure and format of the URLs for your pages. By default, **SuCoS** follows a specific pattern for generating permalinks:

```yaml
"{{ page.SourcePathDirectory }}/{{ page.Title }}"
```

Here, `page.SourcePathDirectory` refers to the directory where the content file is located, and `page.Title` represents the title of the page. This default behavior generates URLs based on the content file's directory and title.

## Front Matter

### URL

To customize the permalink structure, you can modify the url value in the front matter of your content file. For example:

```yaml
---
Title: My Page
URL: my-custom-url
---
```

This is the content of my page.

By setting the url value in the front matter, you can override the [default permalink pattern](#permalink) and define a custom URL for your page.

### Aliases

In addition to permalinks, **SuCoS** also supports aliases. Aliases allow you to define alternative URLs that redirect to the same content. This can be useful when you want to change the URL structure of a page but still maintain backward compatibility with old links.

To specify aliases for a page, you can add an aliases field in the front matter. This field accepts a list of URLs or URL patterns. For example:

```yaml
---
Title: My Page
Aliases:
  - /old-url/
  - "{{ page.Section }}/{{ page.Title }}"
---
```

This is the content of my page.

In the above example, the page "My Page" has two aliases: "/old-url/" and "{{ page.Section }}/{{ page.Title }}".

## Page Variables

**SuCoS** provides various [page variables](/docs/theme/variables/page-variables) that you can use in your content files to access and display dynamic information. These variables allow you to include dynamic elements in your permalinks, aliases, or content. You can check all the [page variables](/docs/theme/variables/page-variables).

Here are some commonly used page variables:

* {{ page.Title }}: The title of the page.
* {{ page.Date }}: The date of the page's creation or last modification.
* {{ page.Params.variableName }}: Custom variables defined in the front matter of the content file.

For example, let's say you have a content file with the following front matter:

```yaml
---
Title: My Page
Date: 2023-06-29
Author: John Doe
---
```

This is the content of my page.

To include the page's date and author in the permalink, you can modify the permalink pattern as follows:

```markdown
"{{ page.Date | date: "%Y-%m-%d" }}/{{ page.Params.Author }}/{{ page.Title }}"
```

In the above permalink pattern, `{{ page.Date | date: "%Y-%m-%d" }}` formats the date as "YYYY-MM-DD", and `{{ page.Params.Author }}` retrieves the value of the custom "author" variable from the front matter.

By leveraging these page variables, you can create dynamic and personalized permalinks, aliases, and content based on the information stored in your content files.

That covers the basic usage of **SuCoS** SSG's permalink and aliases functionalities, along with incorporating page variables. Experiment with these features to create SEO-friendly URLs and maintain backward compatibility with old links while enjoying the flexibility and control offered by **SuCoS**.
