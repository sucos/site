---
Title: Features
---

## General

* **Extremely fast** build times (less than > 1 ms per page)
* Completely cross platform, with [easy installation](/docs/installation) on [Linux](/docs/installation/linux), [Windows](/docs/installation/windows) and more
* Renders changes on the fly with [Live Server](/developers/commands/live-server-command) as you develop
* [Powerful Theming](/docs/theme) with Liquid templates
* Host your site anywhere

## Organization

* Straightforward organization for your projects, including website sections
* [Customizable URLs](/docs/introduction/features/custom-urls#url)
* Sort content as you desire
* Pretty URLs support
* [Permalink pattern](/docs/introduction/features/custom-urls#page-variables) support
* Redirects via [aliases](/docs/introduction/features/custom-urls#aliases)

## Content

* YAML metadata ([frontmatter](/docs/theme/variables/page-variables))
* Customizable homepage
* Multiple content types
