---
Title: Hugo
---

[Hugo](https://gohugo.io/) took the world by storm by offering a SSG with ultra performance.

## Performance

Hugo calls himself as the *"The world’s fastest framework for building websites"*. It is an inflated statement, but not far from the truth.

Our internal benchmarks shows tha **SuCoS** is on par of faster than Hugo in some scenarios.

## Easy to Use

Like SuCoS, Hugo is also a single executable and will generate a site without many extra configurations.

## Template

That is a point that Hugo is really not that good. The template system that it uses tries to implement a natural style of [Go](https://go.dev/) programming language (the same one that Hugo uses... hence the name). It's easy for really simple logic, but it's full of quirks once you adventure on more complex behaviors.

**SuCoS** uses Liquid template system, which is super fast, super powerful and also easy to learn.

## Comparison Table

##

* localization
* built in snippets (Google Analytics, twitter)
