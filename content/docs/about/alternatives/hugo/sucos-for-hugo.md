---
Title: SuCoS for Hugo Users
---

If you already use [Hugo](/docs/introduction/alternatives/hugo), welcome. You're just a step away from exploring the exciting universe of **SuCoS**. As a rule of thumb, consider them interchangeable.

Let's discuss the main differences:

## Site Configuration

In Hugo, the site configuration is managed through a `config` or `hugo` file in the root directory. It supports formats like *YAML*, *TOML*, and *JSON*.

**SuCoS** simplifies site configuration with a `sucos.yaml` file in the root directory. It uses the popular and user-friendly YAML format for configuration.

## Front Matter

Hugo provides flexibility in front matter formats, including *TOML*, *YAML*, and *JSON*.

**SuCoS** exclusively uses *YAML* for front matter. *YAML* is known for its simplicity and readability. All front matter variables are case sensitive and `PascalCase`.

## Templates

Hugo employs a Golang-like template system, which can be complex. It hides the page object and requires direct variable access, such as `{{ .title }}`.

**SuCoS** embraces Liquid as its templating language, offering clarity and direct access to the page object. Variables are accessed using `{{ page.Title }}`. Case sensitivity is maintained with PascalCase.

## Permalink generation

In Hugo, the front matter `URL` allows the usage of certain limited number of tokens. However, the `aliases` do not.

In **SuCoS**, both `URL` and `Aliases` can utilize the same page variables as its template, allowing powerful possibilities.

## Image Processing

Hugo excels in image processing capabilities, allowing resizing and conversion to popular formats like *PNG*, *JPG*, *GIF*, and *WebP*.

**SuCoS** is still in the process of developing image processing capabilities. Stay tuned for updates on this exciting feature.

## Localization Capabilities

Hugo supports localization, enabling the creation of multilingual websites.

Currently, **SuCoS** does not offer built-in localization capabilities. However, **SuCoS** is continuously evolving, and future updates may include this feature.

***

For a detailed comparison of technical differences, refer to the comprehensive table available in our [feature comparison](/docs/introduction/alternatives/hugo/feature-comparison).

As you embark on your **SuCoS** journey, the transition from Hugo becomes a seamless and exciting exploration of new possibilities. While some features are still being developed, **SuCoS** offers a promising alternative with streamlined site configuration, user-friendly front matter, and the power of Liquid templating. Stay tuned for future updates, as **SuCoS** continues to evolve and unlock even more potential for content creators like you.
