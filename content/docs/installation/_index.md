---
Title: Installation
Weight: -1
---

## Requirements

Before we begin, you'll be pleased to know that **SuCoS** has minimal requirements for installation. *In fact, there are none!*

## Steps

Follow these steps to unleash the power of **SuCoS** on your machine. Let's dive right in!

* Once downloaded with the link above, extract the contents of the file to any location on your system.
* Optionally, add the extracted location to your system's PATH environment variable for easy access to the **SuCoS** executable from anywhere in the command prompt. (see either *Linux* and *Windows* help for information how to do this)

And voila! You now have **SuCoS** installed on you machine. Enjoy the power and simplicity of **SuCoS** as you create amazing websites.

### Optional: Git

However, having *Git* installed is recommended for certain advanced features, such as:

* Installing a theme as a Git submodule.
* Accessing commit information from a local Git repository.
* Hosting your site with services like [AWS Amplify], [CloudCannon], [Cloudflare Pages], [GitHub Pages], [GitLab Pages], or [Netlify].
* Building **SuCoS** from source.

If you're planning to utilize any of these features, make sure you have Git installed on your system.

***

If you encounter any issues or need further assistance, don't hesitate to reach out to our support team.

[AWS Amplify]: https://aws.amazon.com/amplify/
[CloudCannon]: https://cloudcannon.com/
[Cloudflare Pages]: https://pages.cloudflare.com/
[GitHub Pages]: https://pages.github.com/
[GitLab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Netlify]: https://www.netlify.com/
