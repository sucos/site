---
Title: Developers
ResourceDefinitions:
  - Src: "*.txt"
    Title: Text
    # Params:
    #   Icon: "asdf"
  - Src: "*.png"
    Title: "{{ page.Title }}-{{counter}}"
    Name: "{{ page.Title }}-{{counter}}"
  - Src: "image/*.jpg"
    Title: "{{ page.Title }}-{{counter}}"
    Name: "{{ page.Title }}-{{counter}}"
  - Src: favicon-32x32.png
    Title: Icon
MainMenu: true
---

As a developer, you hold the keys to unlock **SuCoS**'s full potential. In our developer documentation, you'll discover everything you need to know to leverage **SuCoS**'s features, extend its functionality, and contribute to its growth. From installation instructions to in-depth guides on creating custom themes and plugins, we've got you covered. Let's build some static magic together!

Before we embark on this exciting journey, let's take a moment to appreciate the heart and soul of **SuCoS**: its [**GitLab** repository](https://gitlab.com/sucos/sucos). The repository is a treasure trove of SuCoS's source code, issues, and discussions. Feel free to explore, contribute, and make **SuCoS** even more awesome!

We hope you're as excited as we are about the possibilities **SuCoS** brings to the world of static site generation. Let's embark on this journey together and unleash the true potential of static websites. Happy coding and creating!
